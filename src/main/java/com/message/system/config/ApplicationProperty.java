package com.message.system.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class ApplicationProperty {

    @Value("${notification.app.mail.from-username}")
    private String emailFrom;

    @Value("${email.allowed.flag}")
    private String emailFlag;

    public String getEmailFrom() {
        return emailFrom;
    }
    public String getEmailFlag() {
        return emailFlag;
    }

}
