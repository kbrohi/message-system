package com.message.system.controller;

import com.message.system.dtos.CompanyDto;
import com.message.system.exception.MessagingSystemException;
import com.message.system.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping(value = "/{companyId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public CompanyDto findById(@PathVariable("companyId") Long id) throws MessagingSystemException{
        return companyService.findById(id);
    }

    @GetMapping(value = "/companies", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<CompanyDto> findAll() throws MessagingSystemException{
        return companyService.findAll();
    }


 /*   @GetMapping(value = "/company/{id}/employees", produces = {MediaType.APPLICATION_JSON_VALUE})
    public   List<EmployeeDto> findAllEmployeesByCompanyId(@PathVariable("id")Long id) throws MessagingSystemException{
        return companyService.findAllEmployeesByCompanyId(id);
    }
    @GetMapping(value = "/company/{id}/message-type", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<MessageDto> findAllMessageTypeByCompanyId(@PathVariable("id") Long id) throws MessagingSystemException{
        return companyService.findAllMessageTypeByCompanyId(id);
    }*/
}
