package com.message.system.controller;

import com.message.system.dtos.AuditMessageDto;
import com.message.system.dtos.ContactMethodTypeLookupResDTO;
import com.message.system.dtos.EmployeeContactMethodDto;
import com.message.system.dtos.EmployeeDto;
import com.message.system.exception.MessagingSystemException;
import com.message.system.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired private EmployeeService employeeService;

    @GetMapping(value = "/{employeeCode}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public EmployeeDto findBy(@PathVariable("employeeCode")String employeeCode) throws MessagingSystemException{
        return employeeService.findBy(employeeCode);
    }
    @GetMapping(value = "/employees", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<EmployeeDto> findAll() throws MessagingSystemException{
        return employeeService.findAll();
    }
    @GetMapping(value = "/employees/{companyId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public   List<EmployeeDto> findAllEmployeesByCompanyId(@PathVariable("companyId")Long companyId) throws MessagingSystemException{
        return employeeService.findAllEmployeesByCompanyId(companyId);
    }
    @GetMapping(value = "/contact-method-types", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<ContactMethodTypeLookupResDTO> lookupContactMethodType() throws MessagingSystemException{
        return employeeService.lookupContactMethodType();
    }
    @GetMapping(value = "/{employeeCode}/contact-methods", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<EmployeeContactMethodDto> contactMethodsById(@PathVariable("employeeCode")String employeeCode) throws MessagingSystemException{
        return employeeService.contactMethodsById(employeeCode);
    }
    @PatchMapping(value = "/choose-primary-contact-method/{employeeContactMethodId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public EmployeeContactMethodDto choosePrimaryContactMethod(@PathVariable("employeeContactMethodId") Long employeeContactMethodId) throws MessagingSystemException{
        return employeeService.choosePrimaryContactMethod(employeeContactMethodId);
    }
    @GetMapping(value = "/{employeeCode}/messages", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<AuditMessageDto> findAllMessages(@PathVariable("employeeCode")String employeeCode) throws MessagingSystemException{
        return employeeService.findAllMessages(employeeCode);
    }
    @GetMapping(value = "/health", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String health() throws MessagingSystemException{
        return "I'm Alive";
    }
}
