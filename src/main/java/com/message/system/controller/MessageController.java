package com.message.system.controller;

import com.message.system.dtos.MessageCategoryLookupResDTO;
import com.message.system.dtos.MessageDto;
import com.message.system.dtos.MessageSendDtoReq;
import com.message.system.exception.MessagingSystemException;
import com.message.system.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/notification")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping(value = "/messages", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<MessageDto> findAll() throws MessagingSystemException{
        return messageService.findAll();
    }

    @GetMapping(value = "/messages-by-company/{companyId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<MessageDto> findAllMessageTypeByCompanyId(@PathVariable("companyId")Long companyId) throws MessagingSystemException{
        return messageService.findAllMessageTypeByCompanyId(companyId);
    }
    @GetMapping(value = "/message/{messageId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public MessageDto findById(@PathVariable("messageId")Long id) throws MessagingSystemException{
        return messageService.findById(id);
    }
    @PostMapping(value = "/create-message", produces = {MediaType.APPLICATION_JSON_VALUE})
    public MessageDto createMessage(@RequestBody MessageDto messageDtoReq) throws MessagingSystemException{
        return messageService.createMessage(messageDtoReq);
    }
    @PatchMapping(value = "/update-message", produces = {MediaType.APPLICATION_JSON_VALUE})
    public MessageDto updateMessageType(@RequestBody MessageDto messageDtoReq) throws MessagingSystemException{
        return messageService.updateMessage(messageDtoReq);
    }
    @GetMapping(value = "/message-category-lookup", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<MessageCategoryLookupResDTO> lookupMessageCategory() throws MessagingSystemException{
        return messageService.lookupMessageCategory();
    }
    @PostMapping(value = "/send-message", produces = {MediaType.APPLICATION_JSON_VALUE})
    public MessageDto sendMessageType(@RequestBody MessageSendDtoReq messageTypeDtoReq) throws MessagingSystemException{
        return messageService.sendMessage(messageTypeDtoReq);
    }
}
