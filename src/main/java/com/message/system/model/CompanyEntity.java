package com.message.system.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "COMPANY")
public class CompanyEntity  extends BaseModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "CMP_ID")
    private Long cmpId;
    @Column(name = "NAME")
    private String name;
    @Column(name = "NATURE_OF_BUSINESS")
    private String natureOfBusiness;
}
