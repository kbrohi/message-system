package com.message.system.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "EMPLOYEE_TYPE")
public class EmployeeTypeEntity  extends BaseModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "EMP_TYP_ID")
    private Long empTypeId;
    @Column(name = "EMPLOYEE_TYPE_NAME")
    private String employeeTypeName;
}
