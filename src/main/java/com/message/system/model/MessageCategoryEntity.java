package com.message.system.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "MESSAGE_CATEGORY")
public class MessageCategoryEntity  extends BaseModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
   @Column(name = "MSG_CAT_ID")
    private Long msgCatId;
    @Column(name = "MESSAGE_CATEGORY_NAME")
    private String messageCategoryName;

}
