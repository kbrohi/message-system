package com.message.system.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public class BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "ACTIVE_IND")
    protected Boolean activeInd;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATETIME", nullable = false)
    protected Date createdDatetime;

    @PrePersist
    protected void onCreate() {
        activeInd = activeInd==null?Boolean.TRUE:activeInd;
        createdDatetime = new Date();
    }


    public Boolean getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(Boolean activeInd) {
        this.activeInd = activeInd;
    }

    public Date getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }


}
