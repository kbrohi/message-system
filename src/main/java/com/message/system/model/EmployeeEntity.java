package com.message.system.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "EMPLOYEE")
public class EmployeeEntity  extends BaseModel{
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "EMPLOYEE_TYPE_ID")
    private Long employeeTypeId;
    @Column(name = "COMPANY_ID")
    private Long companyId;
    @Column(name = "FULL_NAME")
    private String fullName;
    @Column(name = "EMPLOYEE_CODE")
    private String employeeCode;
}
