package com.message.system.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "EMPLOYEE_CONTACT_METHOD")
public class EmployeeContactMethodEntity  extends BaseModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "EMPLOYEE_ID")
    private Long employeeId;
    @Column(name = "CONTACT_METHOD_TYPE_ID")
    private Long contactMethodTypeId;
    @Column(name = "CONTACT_METHOD_VALUE")
    private String  contactMethodValue;
    @Column(name = "PRIMARY_IND")
    private Boolean primaryInd;
}
