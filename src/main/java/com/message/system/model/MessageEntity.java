package com.message.system.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "MESSAGE")
public class MessageEntity extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "MESSAGE_CATEGORY_ID")
    private Long messageCategoryId;
    @Column(name = "COMPANY_ID")
    private Long companyId;
    @Column(name = "MESSAGE_NAME")
    private String messageName;
    @Column(name = "MESSAGE_TEMPLATE")
    private String messageTemplate;
    @Column(name = "MESSAGE_SUBJECT")
    private String messageSubject;
}
