package com.message.system.scheduler;


import com.message.system.dtos.MessageSendDtoReq;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class MessageJob implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageJob.class);
    private MessageSendDtoReq messageTypeDto;
    private SendMessageScheduler sendMessageScheduler;

    public MessageJob(MessageSendDtoReq messageTypeDto, SendMessageScheduler sendMessageScheduler){
        this.messageTypeDto = messageTypeDto;
        this.sendMessageScheduler = sendMessageScheduler;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        long start = System.currentTimeMillis();
        if(messageTypeDto==null) {
            messageTypeDto = (MessageSendDtoReq) jobExecutionContext.getJobDetail().getJobDataMap().get("MessageSendDtoReq");
        }
        if(sendMessageScheduler==null) {
            sendMessageScheduler = (SendMessageScheduler) jobExecutionContext.getJobDetail().getJobDataMap().get("SendMessageScheduler");
        }
        LOGGER.info("Job started thread name {} at {}",Thread.currentThread().getName(),new Date(start));
        try {
            sendMessageScheduler.sendSync(messageTypeDto);
        }catch (Exception e){
            LOGGER.error(e.getMessage(),e);
        }
        LOGGER.info("Job completed thread name {} in {}ms",Thread.currentThread().getName(),System.currentTimeMillis() -start);
    }
}
