package com.message.system.scheduler;

import com.message.system.dtos.MessageSendDtoReq;
import com.message.system.exception.MessagingSystemException;
import com.message.system.service.MessageService;
import com.message.system.util.Constants;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.impl.JobDetailImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


@Service
public class SendMessageSchedulerImpl implements SendMessageScheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(SendMessageSchedulerImpl.class);
    @Autowired
    private TaskScheduler taskScheduler;
    @Autowired(required = false)
    Scheduler scheduler;

    @Autowired
    MessageService messageService;

    @Override
    public void scheduleMessage(MessageSendDtoReq messageDto) {
                MessageTask messageTask = new MessageTask(messageDto,this);
                taskScheduler.schedule(messageTask,messageDto.getMessageSendTime());
    }

    @Override
    @Async
    public void sendAync(MessageSendDtoReq messageDto) throws MessagingSystemException {
        messageService.sendMessageToEmployees(messageDto);
    }
    @Override
    public void sendSync(MessageSendDtoReq messageDto) throws MessagingSystemException {
        messageService.sendMessageToEmployees(messageDto);
    }

    public void scheduleCheckExist(MessageSendDtoReq messageDto) throws MessagingSystemException {
        try {
            JobKey jobKey = JobKey.jobKey(messageDto.getId().toString());
            JobDetail jobDetail = null;
            Boolean isJobKeyExist = scheduler.checkExists(jobKey);
            if (isJobKeyExist) {
                jobDetail = scheduler.getJobDetail(jobKey);
                Object o = jobDetail.getJobDataMap().get(Constants.JOB_MESSAGE_SEND_DTO_FIELD);
                if(o!=null && o instanceof MessageSendDtoReq ){
                    MessageSendDtoReq sendDtoReq = (MessageSendDtoReq)o;

                }
            } else {
                JobDetailImpl jobDetailImpl = new JobDetailImpl();
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(Constants.JOB_MESSAGE_SEND_DTO_FIELD,messageDto);
                jobDataMap.put(Constants.JOB_SCHEDULER_FIELD,this);
                jobDetailImpl.setJobDataMap(jobDataMap);
                jobDetailImpl.setJobClass(MessageJob.class);
                jobDetailImpl.setKey(jobKey);
                jobDetail = jobDetailImpl;
            }
            scheduler.addJob(jobDetail, isJobKeyExist);
        }catch (Exception e){
            throw new MessagingSystemException(e.getMessage());
        }
    }

}
