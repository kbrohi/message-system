package com.message.system.scheduler;

import com.message.system.dtos.MessageSendDtoReq;
import com.message.system.exception.MessagingSystemException;

public interface SendMessageScheduler {

    void scheduleMessage(MessageSendDtoReq messageTypeDto) ;
    void sendAync(MessageSendDtoReq messageTypeDto) throws MessagingSystemException;
    void sendSync(MessageSendDtoReq messageTypeDto) throws MessagingSystemException ;
}
