package com.message.system.scheduler;


import com.message.system.dtos.MessageSendDtoReq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class MessageTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageTask.class);
    private MessageSendDtoReq messageTypeDto;
    private SendMessageScheduler sendMessageScheduler;

    public MessageTask(MessageSendDtoReq messageTypeDto, SendMessageScheduler sendMessageScheduler){
        this.messageTypeDto = messageTypeDto;
        this.sendMessageScheduler = sendMessageScheduler;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        LOGGER.info("Job started thread name {} at {}",Thread.currentThread().getName(),new Date(start));
        try {
            sendMessageScheduler.sendSync(messageTypeDto);
        }catch (Exception e){
            LOGGER.error(e.getMessage(),e);
        }
        LOGGER.info("Job completed thread name {} in {}ms",Thread.currentThread().getName(),System.currentTimeMillis() -start);
    }
}
