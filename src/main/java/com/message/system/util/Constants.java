package com.message.system.util;


public class Constants {
    public static final String STATUS_SUCCESS="Success";
    public static final String STATUS_FAIL="Fail";
    public static final Long CONTACT_TYPE_EMAIL=1L;
    public static final Long CONTACT_TYPE_SMS=2L;
    public static final Long MESSAGE_CATEGORY_EMAIL=1L;
    public static final Long MESSAGE_CATEGORY_SMS=2L;
    public static final String JOB_MESSAGE_SEND_DTO_FIELD= "MessageSendDtoReq";
    public static final String JOB_SCHEDULER_FIELD= "MessageSendDtoReq";
}
