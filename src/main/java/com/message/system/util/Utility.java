package com.message.system.util;

import org.springframework.lang.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


public final class Utility {
    private static final Logger logger = Logger.getLogger(Logger.class.getName());

    public static final String MM_DD_YYYY_HH_mm_ss = "MM/dd/yyyy HH:mm:ss";
    public static final String EEE_MMM_DD_HH_MM_SS_Z_YYYY = "EEE MMM dd HH:mm:ss z yyyy";
    public static final String YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DD_MM_YYYY_HH_MM_SS = "dd-MM-yyyy HH:mm:ss";
    public static final String DD_MM_YYYY_HH_MM_SS_SLASH = "dd/MM/yyyy HH:mm:ss a";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String DDMMYYYY_TIMESTAMP = "dd/MM/yyyy hh:mm a";
    public static final String YYYYMMDD = "yyyyMMdd";
    public static final String YYMM = "yyMM";
    public static final String DD_MM_YYYY_SLASH = "dd/MM/yyyy";
    public static final String DD_MM_YYYY_SLASH_TIMESTAMP = "dd/MM/yyyy hh:mm:ss";
    public static final String YYYYMMDD_HHMMSS = "yyyyMMdd hhmmss";
    public static final String DD_MMM_YYYY_TIMESTAMP = "dd MMM, yyyy hh:mm aaa";
    public static final String DB_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss.S";
    public static final String DB_DATE_FORMAT = "yyyy-MM-dd";
    public static final String BPM_DATE_FORMAT = "yyyy-MM-dd";
    public static final String BPM_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String BPM_TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DD_MM_YYYY = "dd MMM yyyy";
    public static final String MMMM_YYYY = "MMMM YYYY";
    public static final String MMMM = "MMMM";
    public static final String MMDDYYYY = "MMDDYYYY";
    public static final String hhmmss = "hhmmss";
    public static final String DD_MM_YYYY_DATE_FORMAT = "dd-MM-yyyy";
    public static final String YYYY_MM_DD_T_HH_MM_SS_SSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String YYYY_MM_DD_T_HH_MM_SS_HH_MM = "yyyy-MM-dd'T'HH:mm:ss+hh:mm";

    private final static String CRON_EXPRESSION_SEPERATOR = " ";


   public static String getCronExpression(String time) {
        String[] timeSplit = time.split(":");
        //0 35 22 0/1 * ? *
        if (timeSplit.length == 3) {
            return new StringBuilder().append(
                    timeSplit[2]).append(CRON_EXPRESSION_SEPERATOR)
                    .append(timeSplit[1]).append(CRON_EXPRESSION_SEPERATOR).append(timeSplit[0])
                    .append(CRON_EXPRESSION_SEPERATOR).append("0/1 * *").toString();
        }
        return null;
    }
    public static final String convertDateToString(Date aDate,
                                                   String formatToBeConverted) {
        SimpleDateFormat df = null;
        String returnValue = "";

        try {
            if (aDate != null) {
                df = new SimpleDateFormat(formatToBeConverted);
                returnValue = df.format(aDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
            returnValue = "";
        }

        return (returnValue);
    }

    public static final String convertDateToString(String aDate,
                                                   String aDatePattern, String formatToBeConverted) {
        SimpleDateFormat df = null;
        String returnValue = "";

        try {
            if (aDate != null && !aDate.equals("")) {
                df = new SimpleDateFormat(aDatePattern);
                Date newDate = df.parse(aDate);
                if (newDate != null) {
                    df = new SimpleDateFormat(formatToBeConverted);
                    returnValue = df.format(newDate);
                }
            }
        } catch (Exception e) {
            returnValue = "";
        }

        return (returnValue);
    }
    public static Date parseToDateWithoutFormat(String stringOfDate) throws ParseException {
        if (stringOfDate != null && !stringOfDate.isEmpty()) {
            SimpleDateFormat[] formats = new SimpleDateFormat[] {new SimpleDateFormat(YYYY_MM_DD_T_HH_MM_SS), new SimpleDateFormat(YYYY_MM_DD_T_HH_MM_SS_SSSZ),
                            new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS), new SimpleDateFormat(YYYY_MM_DD_T_HH_MM_SS_HH_MM), new SimpleDateFormat(DB_DATETIME_FORMAT),
                            new SimpleDateFormat(DB_DATE_FORMAT), new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS), new SimpleDateFormat(DD_MM_YYYY_SLASH), new SimpleDateFormat(DD_MM_YYYY_SLASH_TIMESTAMP),
                            new SimpleDateFormat(DD_MMM_YYYY_TIMESTAMP), new SimpleDateFormat(DD_MM_YYYY_DATE_FORMAT), new SimpleDateFormat(DD_MM_YYYY), new SimpleDateFormat(BPM_DATE_FORMAT),
                            new SimpleDateFormat(BPM_DATE_TIME_FORMAT), new SimpleDateFormat(BPM_TIMESTAMP_FORMAT), new SimpleDateFormat(DDMMYYYY_TIMESTAMP), new SimpleDateFormat(MMMM_YYYY),
                            new SimpleDateFormat(YYYYMMDD), new SimpleDateFormat(YYMM), new SimpleDateFormat(YYYYMMDD_HHMMSS), new SimpleDateFormat(EEE_MMM_DD_HH_MM_SS_Z_YYYY)};
            Date parsedDate;
            for (SimpleDateFormat format : formats) {
                try {
                    logger.log(Level.INFO, "Parsing Using Format: " + format);
                    parsedDate = format.parse(stringOfDate);
                    return parsedDate;
                } catch (ParseException e) {
                    e.printStackTrace();
                    logger.log(Level.INFO, "Could not parse the date: " + stringOfDate);
                }
            }
        }
        throw new ParseException("Unknown Date Format: '" + stringOfDate + "'", 0);
    }


    public static Timestamp parseToTimestampWithoutFormat(String stringOfDate) throws ParseException {
        return getTimestamp(parseToDateWithoutFormat(stringOfDate));
    }


    public static String formatDate(Date dateTime, String format) {
        if (dateTime == null)
            return "";

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(dateTime);
    }

    public static boolean lessThanEqual(Date source, Date target) {
        boolean lessThanEqual = false;

        if (source != null && target != null) {
            lessThanEqual = target.after(source) || target.equals(source);
        }

        return lessThanEqual;
    }

    public static boolean greaterThanEqual(Date source, Date target) {
        boolean greaterThanEqual = false;

        if (source != null && target != null) {
            greaterThanEqual = target.before(source) || target.equals(source);
        }

        return greaterThanEqual;
    }
    
    public static boolean isDateBefore(Date source, Date target) {
        boolean isDateBefore = false;

        if (source != null && target != null) {
            isDateBefore = source.before(target);
        }

        return isDateBefore;
    }

    public static Date addHours(Date currentDate, int noOfHours) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.HOUR, noOfHours);

        return cal.getTime();
    }

    public static Date addMins(Date currentDate, int noOfMins) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MINUTE, noOfMins);

        return cal.getTime();
    }

    public static Date addDays(Date currentDate, int noOfDays) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DATE, noOfDays);

        return cal.getTime();
    }

    public static Timestamp addDays(Timestamp currentDate, int noOfDays) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DATE, noOfDays);

        return new Timestamp(cal.getTime().getTime());
    }

    public static Date subtractDays(Date currentDate, int noOfDays) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DATE, (-1 * noOfDays));

        return cal.getTime();
    }

    public static Timestamp subtractDays(Timestamp currentDate, int noOfDays) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DATE, (-1 * noOfDays));

        return new Timestamp(cal.getTime().getTime());
    }

    public static Date addMonths(Date currentDate, int noOfMonths) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MONTH, noOfMonths);

        return cal.getTime();
    }

    public static Date subtractMonths(Date currentDate, int noOfMonths) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MONTH, (-1 * noOfMonths));

        return cal.getTime();
    }

    public static Date addYears(Date currentDate, int noOfYears) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.YEAR, noOfYears);

        return cal.getTime();
    }

    public static Date getPreDate(Date currentDate, int noOfYears) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.YEAR, noOfYears);

        return cal.getTime();
    }

    public static long getDateDifference(Date currentDate, Date pastDate, TimeUnit timeUnit) {
        long diffInMillies = currentDate.getTime() - pastDate.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

    }

    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    public static int getAge(Date dateOfBirth) {
        Calendar now = Calendar.getInstance();
        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);

        if (dob.after(now)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }
        int year1 = now.get(Calendar.YEAR);
        int year2 = dob.get(Calendar.YEAR);
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = dob.get(Calendar.MONTH);
        if (month2 > month1) {
            age--;
        } else if (month1 == month2) {
            int day1 = now.get(Calendar.DAY_OF_MONTH);
            int day2 = dob.get(Calendar.DAY_OF_MONTH);
            if (day2 > day1) {
                age--;
            }
        }
        return age;
    }

    public static Date getCurrentYearStartDate() {
        return getStartDateOfYear(Calendar.getInstance().get(Calendar.YEAR));
    }

    public static Date getCurrentYearEndDate() {
        return getEndDateOfYear(Calendar.getInstance().get(Calendar.YEAR));
    }


    /**
     * @param date
     * @return Timestamp
     */
    public static Timestamp getTimestamp(Date date) {
        if (date != null) {
            return new Timestamp(date.getTime());
        }
        return null;
    }

    /**
     * @return Timestamp
     */
    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(new Date().getTime());
    }

    /**
     * @return Current Date
     */
    public static Date getCurrentDate() {
        return new Date();
    }

    /**
     * Set hh:mm:ss to 23:59:59
     *
     * @param date
     * @return Bumped Date
     */
    public static Date getEndDateOfMonth(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            return calendar.getTime();
        }
        return null;
    }

    /**
     * Set hh:mm:ss to 23:59:59
     *
     * @param date
     * @return Bumped Date
     */
    public static Date getEndTime(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            return calendar.getTime();
        }
        return null;
    }

    /**
     * Set hh:mm:ss to 00:00:00
     *
     * @param date
     * @return Bumped Date
     */

    public static Date getStartDateOfMonth(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            return calendar.getTime();
        }
        return null;
    }


    /**
     * Subtract one month from date
     *
     * @param date
     * @return Bumped Date
     */
    @Nullable
    public static Date getPreviousMonth(@Nullable Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, -1);
            return calendar.getTime();
        }
        return null;
    }

    public static Date getDateByMonthAndYear(Integer month, Integer year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year.intValue());
        calendar.set(Calendar.MONTH, month.intValue() - 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    public static Date getStartDateOfYear(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    public static Date getEndDateOfYear(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }

    public static Date clearTimeFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.AM_PM, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }


    public static Date getNextDateWithStartTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, +1);
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
        return new Timestamp(cal.getTime().getTime());
    }

    public static Date getPreviousDateWithEndTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMaximum(Calendar.MILLISECOND));
        return new Timestamp(cal.getTime().getTime());
    }

    public static Date getMaxTimeOfDay(Date date) {
        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMaximum(Calendar.MILLISECOND));
        return cal.getTime();
    }

    public static Date getStartTimeOfDay(Date date) {
        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
        return cal.getTime();
    }

    public static int getAge(Date dateOfBirth, Date endDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(endDate);
        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);

        if (dob.after(now)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }
        int year1 = now.get(Calendar.YEAR);
        int year2 = dob.get(Calendar.YEAR);
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = dob.get(Calendar.MONTH);
        if (month2 > month1) {
            age--;
        } else if (month1 == month2) {
            int day1 = now.get(Calendar.DAY_OF_MONTH);
            int day2 = dob.get(Calendar.DAY_OF_MONTH);
            if (day2 > day1) {
                age--;
            }
        }
        return age;
    }

    public static Integer getPrvMonthWorkingDays(Date date) {
        Integer workingDays = 0;
        if (date != null) {
            Calendar start = Calendar.getInstance();
            start.setTime(date);
            start.add(Calendar.MONTH, -1);
            start.set(Calendar.DATE, start.getActualMinimum(Calendar.DAY_OF_MONTH));

            Calendar end = Calendar.getInstance();
            end.setTime(date);
            end.add(Calendar.MONTH, -1);
            end.set(Calendar.DATE, end.getActualMaximum(Calendar.DAY_OF_MONTH));

            workingDays = getWorkingDays(start, end);
        }
        return workingDays;
    }

    public static Integer getPrvMonthNumber(Date date) {
        Integer month = 0;
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MONTH, -1);
            month = cal.get(Calendar.MONTH);
            month++;
        }
        return month;
    }

    public static Integer getWorkingDays(Date fromDate, Date toDate) {
        Integer workingDays = 0;
        if (fromDate != null && toDate != null) {
            Calendar start = Calendar.getInstance();
            start.setTime(fromDate);

            Calendar end = Calendar.getInstance();
            end.setTime(toDate);
            workingDays = getWorkingDays(start, end);
        }
        return workingDays;
    }

    private static Integer getWorkingDays(Calendar start, Calendar end) {
        Integer workingDays = 0;
        while (!start.after(end)) {
            int day = start.get(Calendar.DAY_OF_WEEK);
            if (day != Calendar.SUNDAY && day != Calendar.SATURDAY) {
                workingDays++;
            }
            start.add(Calendar.DATE, 1);
        }
        return workingDays;
    }

    public static Timestamp convertStringToTimestamp(String dateString) {
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat(DB_DATETIME_FORMAT);
            Date date = formatter.parse(dateString);
            return getTimestamp(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static XMLGregorianCalendar getGregorianCalendarDate(Date d) throws DatatypeConfigurationException {
        XMLGregorianCalendar xmlGregorianCalendar = null;
        if (d != null) {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(d);
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        }
        return xmlGregorianCalendar;
    }

    public static Calendar setTimeAttrsToZero(Calendar calendar) {
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar;
    }

    public static Timestamp resetTimeAttributes(Timestamp date) {
        Calendar cal = Calendar.getInstance(); // locale-specific
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTimeInMillis());
    }

    public static Date resetTimeAttributes(Date date) {
        Calendar cal = Calendar.getInstance(); // locale-specific
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Date(cal.getTimeInMillis());
    }

    public static String getDayName(Date inputDate, String format) throws ParseException {
        return new SimpleDateFormat("EEEE", Locale.ENGLISH).format(new SimpleDateFormat(format).parse(inputDate.toString())).toUpperCase();
    }

    public static int getDayOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static String getMonthAndYear(Date date) {
        Format formatter = new SimpleDateFormat(MMMM_YYYY);
        return formatter.format(date);
    }

    public static String getMonth(Date date) {
        Format formatter = new SimpleDateFormat(MMMM);
        return formatter.format(date);
    }

    public static boolean isDateInBetween(Date startDate, Date currentDate, Date endDate) {
        boolean isDateInBetween = false;
        if (startDate != null && currentDate != null && endDate != null) {
            isDateInBetween = (currentDate.after(startDate) || currentDate.equals(startDate)) && (endDate.after(currentDate) || endDate.equals(currentDate));
        }
        return isDateInBetween;
    }

    public static Date subtractMins(Date currentDate, int noOfMins) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MINUTE, -noOfMins);

        return cal.getTime();
    }

    public static Long getMonthsBtwDates(Date date1, Date date2) {

        Instant instant1 = date1.toInstant();
        LocalDate localDate1 = instant1.atZone(ZoneId.systemDefault()).toLocalDate();

        Instant instant2 = date2.toInstant();
        LocalDate localDate2 = instant2.atZone(ZoneId.systemDefault()).toLocalDate();

        java.time.Period period = java.time.Period.between(localDate2, localDate1);
        Long months = (long) period.getMonths();
        Long years = (long) period.getYears();
        Long totalMonths = null;

        if (years != 0) {
            totalMonths = months + (years * 12);
        } else {
            totalMonths = months;
        }
        return totalMonths;
    }

    public static Boolean isDateInRange(Date fromRange, Date toRange, Date dateInBetween) {

        Boolean fromDateIsGreater = greaterThanEqual(dateInBetween, fromRange);

        Boolean toDateIsGreater = lessThanEqual(dateInBetween, toRange);
        System.out.println("fromDateIsGreater[" + (fromDateIsGreater) + "]");
        System.out.println("toDateIsGreater[" + (toDateIsGreater) + "]");

        return fromDateIsGreater && toDateIsGreater;
    }

    public static void main(String[] a) throws ParseException {

        String input = "10:22 PM";
        DateFormat df = new SimpleDateFormat("hh:mm aa");
        DateFormat outputformat = new SimpleDateFormat("HH:mm");
        Date date;
        String output;
        date = df.parse(input);
        output = outputformat.format(date);
        System.out.println(output);

    }


    public static Date getIsoDate() throws ParseException {
        String iso = "yyyy-MM-dd'T'HH:mm:ss'.000000000+00:00'";
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat(iso);
        df.setTimeZone(tz);
        return df.parse(df.format(new Date()));

    }

    public static Date convertStringTimeToDate(String date, String format) throws ParseException {
        DateFormat df = new SimpleDateFormat(format);
        return df.parse(date);
    }


}
