package com.message.system.exception;

public class MessagingSystemException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;


    private String message;

    public MessagingSystemException() {}

    public MessagingSystemException(String message) {
        this(message, null);
    }

    public MessagingSystemException(String message, Throwable t) {
        super(message, t);
        this.message = message;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
