package com.message.system.exception;

import com.message.system.dtos.CommonDto;
import com.message.system.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;



@ControllerAdvice
public class MessagingSystemGeneralExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessagingSystemGeneralExceptionHandler.class);

    private static final HttpHeaders HEADERS = new HttpHeaders();

    @SuppressWarnings("rawtypes")
    @ExceptionHandler(MessagingSystemException.class)
    @ResponseBody
    public ResponseEntity<CommonDto> handleDynamicQRClientException(MessagingSystemException ex, WebRequest request) {
        return logAndReturn(ex,  ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @SuppressWarnings("rawtypes")
    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
    public ResponseEntity<CommonDto> handleDataIntegrityException(DataIntegrityViolationException ex, WebRequest request) {
        return logAndReturn(ex,  ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @SuppressWarnings("rawtypes")
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public ResponseEntity<CommonDto> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        return logAndReturn(ex,  ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private ResponseEntity<CommonDto> logAndReturn(Throwable t, String errorMessage,  HttpStatus httpStatus) {
        LOGGER.error("Processing " + t.getClass().getName());
        HEADERS.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return new ResponseEntity<>(new CommonDto(errorMessage,Constants.STATUS_FAIL), HEADERS, httpStatus);
    }
}
