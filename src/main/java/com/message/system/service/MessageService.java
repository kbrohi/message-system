package com.message.system.service;

import com.message.system.dtos.MessageCategoryLookupResDTO;
import com.message.system.dtos.MessageDto;
import com.message.system.dtos.MessageSendDtoReq;
import com.message.system.exception.MessagingSystemException;

import java.util.List;

public interface MessageService {

    List<MessageDto> findAll() throws MessagingSystemException;
    List<MessageDto> findAllMessageTypeByCompanyId(Long companyId) throws MessagingSystemException;
    MessageDto findById(Long id) throws MessagingSystemException;
    MessageDto createMessage(MessageDto messageDtoReq) throws MessagingSystemException;
    MessageDto updateMessage(MessageDto messageDtoReq) throws MessagingSystemException;
    List<MessageCategoryLookupResDTO> lookupMessageCategory() throws MessagingSystemException;
    MessageDto sendMessage(MessageSendDtoReq messageTypeDtoReq) throws MessagingSystemException;
    void sendMessageToEmployeesAsync(MessageSendDtoReq reqDto) throws MessagingSystemException;
    void sendMessageToEmployees(MessageSendDtoReq reqDto) throws MessagingSystemException;
    boolean sendMessageWithAudit( MessageSendDtoReq req) throws MessagingSystemException;
    void saveAuditMessage(MessageSendDtoReq req) throws MessagingSystemException ;
}
