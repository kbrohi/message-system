package com.message.system.service;


import com.message.system.dtos.CompanyDto;
import com.message.system.dtos.EmployeeDto;
import com.message.system.dtos.MessageDto;
import com.message.system.exception.MessagingSystemException;

import java.util.List;

public interface CompanyService {

    CompanyDto findById(Long id) throws MessagingSystemException;
    CompanyDto findByCmpId(Long id) throws MessagingSystemException;
    List<CompanyDto> findAll() throws MessagingSystemException;


    List<EmployeeDto> findAllEmployeesByCompanyId(Long companyId) throws MessagingSystemException;

    List<MessageDto> findAllMessageTypeByCompanyId(Long companyId) throws MessagingSystemException;
}
