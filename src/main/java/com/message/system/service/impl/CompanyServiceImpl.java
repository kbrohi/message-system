package com.message.system.service.impl;


import com.message.system.dtos.CompanyDto;
import com.message.system.dtos.EmployeeDto;
import com.message.system.dtos.MessageDto;
import com.message.system.dtos.mapper.CompanyMapper;
import com.message.system.exception.MessagingSystemException;
import com.message.system.model.CompanyEntity;
import com.message.system.repository.CompanyRepository;
import com.message.system.service.CompanyService;
import com.message.system.service.EmployeeService;
import com.message.system.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private MessageService messageService;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private EmployeeService employeeService;

    @Override
    public CompanyDto findById(Long id) throws MessagingSystemException {
        CompanyDto companyDto = null;
        Optional<CompanyEntity> companyEntityOptional = companyRepository.findById(id);
        if (companyEntityOptional.isPresent()) {
            companyDto = companyMapper.companyEntityToDto(companyEntityOptional.get());
        }else{
            throw  new MessagingSystemException("Record Not Found");
        }
        return companyDto;
    }
    @Override
    public CompanyDto findByCmpId(Long id) throws MessagingSystemException {
        CompanyDto companyDto = null;
        Optional<CompanyEntity> companyEntityOptional = companyRepository.findByCmpId(id);
        if (companyEntityOptional.isPresent()) {
            companyDto = companyMapper.companyEntityToDto(companyEntityOptional.get());
        }else{
            throw  new MessagingSystemException("Record Not Found");
        }
        return companyDto;
    }

    @Override
    public List<CompanyDto> findAll() throws MessagingSystemException{
       List<CompanyDto> companyDtos = null;
        List<CompanyEntity> companyEntityOptional = companyRepository.findAll();
        if (companyEntityOptional!=null && companyEntityOptional.size()>0) {
            companyDtos = companyMapper.listCompanyEntityToDto(companyEntityOptional);
        }else{
            throw  new MessagingSystemException("Record Not Found");
        }
        return companyDtos;
    }

    @Override
    public List<EmployeeDto> findAllEmployeesByCompanyId(Long companyId) throws MessagingSystemException {
        return employeeService.findAllEmployeesByCompanyId(companyId);
    }
    @Override
    public List<MessageDto> findAllMessageTypeByCompanyId(Long companyId) throws MessagingSystemException {
        return  messageService.findAllMessageTypeByCompanyId(companyId);
    }
}
