package com.message.system.service.impl;

import com.message.system.dtos.MessageCategoryLookupResDTO;
import com.message.system.dtos.MessageDto;
import com.message.system.dtos.MessageSendDtoReq;
import com.message.system.dtos.mapper.MessageTypeMapper;
import com.message.system.exception.MessagingSystemException;
import com.message.system.model.AuditMessageEntity;
import com.message.system.model.CompanyEntity;
import com.message.system.model.MessageCategoryEntity;
import com.message.system.model.MessageEntity;
import com.message.system.repository.*;
import com.message.system.scheduler.SendMessageScheduler;
import com.message.system.service.EmailService;
import com.message.system.service.MessageService;
import com.message.system.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class MessageServiceImpl implements MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);
    public static final String SEND_MODE_TYPE_IMMEDIATE = "immediate";
    public static final String SEND_MODE_TYPE_NIGHTLY = "nightly";
    public static final String SEND_MODE_TYPE_CUSTOM_MESSAGE_TYPE = "custom_message_type";
    public static final String SEND_MODE_TYPE_CUSTOM_MESSAGE_TYPE_TO_EMAIL = "custom_message_type_to_email";


    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private MessageCategoryRepository messageCategoryRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private MessageTypeMapper messageTypeMapper;

    @Autowired
    private EmailService emailService;

    @Autowired(required = false)
    @Lazy
    private SendMessageScheduler sendMessageScheduler;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AuditMessageTypeRepository auditMessageTypeRepository;

    @Override
    public List<MessageDto> findAll() throws MessagingSystemException{

        List<MessageDto> messageDtos = null;
        List<MessageEntity> messageTypeEntitiesOpl =  messageRepository.findAll();
        if(messageTypeEntitiesOpl ==null || messageTypeEntitiesOpl.size()==0){
            throw new MessagingSystemException("Record Not Found");

        }
        messageDtos = messageTypeMapper.listMessageTypeEntityToDto(messageTypeEntitiesOpl);
        messageDtos.forEach(e->{
            populateMessageTypeNameAndCompanyName(e);
        });
        return messageDtos;
    }
    @Override
    public List<MessageDto> findAllMessageTypeByCompanyId(Long companyId) throws MessagingSystemException{
        List<MessageDto> messageDtos = null;
        List<MessageEntity> messageTypeEntitiesOpl =  messageRepository.findAllByCompanyId(companyId);
        if(messageTypeEntitiesOpl==null ||messageTypeEntitiesOpl.size()==0){
            throw new MessagingSystemException("Record Not Found");
        }
        messageDtos = messageTypeMapper.listMessageTypeEntityToDto(messageTypeEntitiesOpl);
        messageDtos.forEach(e->{
            populateMessageTypeNameAndCompanyName(e);
        });

        return messageDtos;
    }
    @Override
    public MessageDto createMessage(MessageDto messageDtoReq) throws MessagingSystemException{
        MessageDto messageDtoResp = null;
        MessageEntity messageEntity = null;
        Optional<MessageEntity> messageTypeEntityOptional=  messageRepository.findByMessageNameAndMessageCategoryId(
                messageDtoReq.getMessageName(), messageDtoReq.getMessageCategoryId()
        );
        if(messageTypeEntityOptional.isPresent()){
            messageEntity =  messageTypeEntityOptional.get();
            String oldMessageType = messageEntity.getMessageTemplate();
            messageTypeMapper.updateMessageTypeDtoToEntity(messageDtoReq, messageEntity);
            oldMessageType = oldMessageType+"<br/>" + messageEntity.getMessageTemplate();
            messageEntity.setMessageTemplate(oldMessageType);
        }else{
            messageEntity = messageTypeMapper.messageTypeDtoToEntity(messageDtoReq);
        }

        messageRepository.save(messageEntity);
        messageDtoResp = messageTypeMapper.messageTypeEntityToDto(messageEntity);
        messageDtoResp = populateMessageTypeNameAndCompanyName(messageDtoResp);
        return messageDtoResp;
    }

    @Override
    public MessageDto findById(Long id) throws MessagingSystemException{
        MessageDto messageDto = null;
        Optional<MessageEntity> messageTypeEntityOptional = messageRepository.findById(id);

        if(!messageTypeEntityOptional.isPresent()){
            throw new MessagingSystemException("Message Type Not Found Against ["+id+"]");
        }

        MessageEntity messageEntity = messageTypeEntityOptional.get();
        messageDto = messageTypeMapper.messageTypeEntityToDto(messageEntity);
        messageDto = populateMessageTypeNameAndCompanyName(messageDto);
        return messageDto;
    }

    private MessageDto populateMessageTypeNameAndCompanyName( MessageDto messageDto) {
        Optional<MessageCategoryEntity> categoryEntityOp =  messageCategoryRepository.findById(messageDto.getMessageCategoryId());
        if(categoryEntityOp.isPresent()){
            messageDto.setMessageCategoryName(categoryEntityOp.get().getMessageCategoryName());
        }
        Optional<CompanyEntity> companyEntity =  companyRepository.findById(messageDto.getCompanyId());
        if(companyEntity.isPresent()){
            messageDto.setCompanyName(companyEntity.get().getName());
        }
        return messageDto;
    }

    @Override
    public List<MessageCategoryLookupResDTO> lookupMessageCategory() throws MessagingSystemException{
        List<MessageCategoryLookupResDTO> messageCategoryLookupResDTOS = messageCategoryRepository.lookupActive();
        return messageCategoryLookupResDTOS;
    }
    @Override
    public MessageDto updateMessage(MessageDto messageDtoReq) throws MessagingSystemException{
        MessageDto messageDtoResp = null;
        if(messageDtoReq ==null || messageDtoReq.getId() == null){
            throw  new MessagingSystemException("Please provide valid Request");
        }
        Optional<MessageEntity> messageTypeEntityOptional=  messageRepository.findById(messageDtoReq.getId());
        if(!messageTypeEntityOptional.isPresent()){
            throw  new MessagingSystemException("Please provide valid Request MessageType Not Found against["+ messageDtoReq.getId()+"]");
    }
        MessageEntity messageEntity = messageTypeEntityOptional.get();
        messageTypeMapper.updateMessageTypeDtoToEntity(messageDtoReq, messageEntity);
        messageRepository.save(messageEntity);
        messageDtoResp = messageTypeMapper.messageTypeEntityToDto(messageEntity);
        messageDtoResp = populateMessageTypeNameAndCompanyName(messageDtoResp);
        return messageDtoResp;
    }

    @Override
    public MessageDto sendMessage(MessageSendDtoReq messageTypeDtoReq) throws MessagingSystemException{
        MessageDto messageDtoResp = null;
        if(messageTypeDtoReq == null || StringUtils.isEmpty(messageTypeDtoReq.getSendModeTypeId())
                ){
            throw  new MessagingSystemException("Please provide valid Request");
        }

        if((!SEND_MODE_TYPE_CUSTOM_MESSAGE_TYPE_TO_EMAIL.equalsIgnoreCase(messageTypeDtoReq.getSendModeTypeId()))
                &&( messageTypeDtoReq.getCompanyId()==null &&
                messageTypeDtoReq.getEmployeeId()==null &&
                messageTypeDtoReq.getEmployeeTypeId() == null)
                ){
            throw  new MessagingSystemException("Please Provide either CompanyId, EmployeeId or EmployeeTypeId");
        }

        if(SEND_MODE_TYPE_CUSTOM_MESSAGE_TYPE.equalsIgnoreCase(messageTypeDtoReq.getSendModeTypeId())){
            if(StringUtils.isEmpty(messageTypeDtoReq.getMessageTemplate())){
                throw  new MessagingSystemException("Please Provide Message Text");
            }

        }
        if(SEND_MODE_TYPE_CUSTOM_MESSAGE_TYPE_TO_EMAIL.equalsIgnoreCase(messageTypeDtoReq.getSendModeTypeId())){
            if(StringUtils.isEmpty(messageTypeDtoReq.getMessageTemplate()) ||
                    StringUtils.isEmpty(messageTypeDtoReq.getToEmailAddress()) ){
                throw  new MessagingSystemException("Please Provide Either Message Text or To Email Address");
            }
            sendMessageWithAudit(messageTypeDtoReq);
            messageDtoResp = new MessageDto();
            messageDtoResp.setStatus(Constants.STATUS_SUCCESS);
            return messageDtoResp;
        }

        if(SEND_MODE_TYPE_NIGHTLY.equalsIgnoreCase(messageTypeDtoReq.getSendModeTypeId())){
            if(messageTypeDtoReq.getMessageSendTime() ==null ){
                throw  new MessagingSystemException("Please Provide Message Time in datetime UTC");
            }
            MessageSendDtoReq messageTypeDtoReqNew=null;
            messageTypeDtoReqNew = getMessageTypeSendDtoReqFromDB(messageTypeDtoReq);
            sendMessageScheduler.scheduleMessage(messageTypeDtoReqNew);
            messageDtoResp = new MessageDto();
            messageDtoResp.setStatus(Constants.STATUS_SUCCESS);
            messageDtoResp.setMessage("Send Message Types Request has been successfully Scheduled");
        }else if (SEND_MODE_TYPE_IMMEDIATE.equalsIgnoreCase(messageTypeDtoReq.getSendModeTypeId())){
            MessageSendDtoReq messageTypeDtoReqNew=null;
            messageTypeDtoReqNew = getMessageTypeSendDtoReqFromDB(messageTypeDtoReq);
            sendMessageScheduler.sendAync(messageTypeDtoReqNew);
            messageDtoResp = new MessageDto();
            messageDtoResp.setStatus(Constants.STATUS_SUCCESS);
            messageDtoResp.setMessage("Send Message Types Request has been successfully sent");
        }
        return messageDtoResp;
    }

    private MessageSendDtoReq getMessageTypeSendDtoReqFromDB(MessageSendDtoReq messageTypeDtoReq) throws MessagingSystemException {
        MessageSendDtoReq messageTypeDtoReqNew;Optional<MessageEntity> messageTypeEntityOptional = messageRepository.findById(messageTypeDtoReq.getId());
        if(!messageTypeEntityOptional.isPresent()){
            throw  new MessagingSystemException("Message Type Not Found Id["+messageTypeDtoReq.getId()+"]");
        }
        messageTypeDtoReqNew  = messageTypeMapper.messageTypeEntityToSendDto(messageTypeEntityOptional.get());
        messageTypeDtoReqNew.setCompanyId(messageTypeDtoReq.getCompanyId());
        messageTypeDtoReqNew.setEmployeeId(messageTypeDtoReq.getEmployeeId());
        messageTypeDtoReqNew.setEmployeeTypeId(messageTypeDtoReq.getEmployeeTypeId());
        messageTypeDtoReqNew.setToEmailAddress(messageTypeDtoReq.getToEmailAddress());
        messageTypeDtoReqNew.setMessageSendTime(messageTypeDtoReq.getMessageSendTime());
        return messageTypeDtoReqNew;
    }
    @Async
    @Override
    public void sendMessageToEmployeesAsync(MessageSendDtoReq reqDto) throws MessagingSystemException{
        sendMessageToEmployees(reqDto);
    }
    @Override
    public void sendMessageToEmployees(MessageSendDtoReq reqDto) throws MessagingSystemException{
        List<MessageSendDtoReq> sendDtoReqsOpl  = employeeRepository.findAllByCompanyIdOrIdOrEmployeeTypeId(reqDto.getCompanyId(),reqDto.getEmployeeId(),reqDto.getEmployeeTypeId());
        if(sendDtoReqsOpl !=null){
            List<MessageSendDtoReq> messageSendDtoReqs = sendDtoReqsOpl;
            for(MessageSendDtoReq req : messageSendDtoReqs){
                req.setId(reqDto.getId());
                req.setMessageSubject(reqDto.getMessageSubject());
                req.setMessageTemplate(reqDto.getMessageTemplate());
                req.setMessageCategoryId(reqDto.getMessageCategoryId());
                sendMessageWithAudit(req);
            }

        }else{
            LOGGER.warn("Message Type Not Found for ["+reqDto+"]");
            throw  new MessagingSystemException("Message Type Not Found for ["+reqDto+"]");
        }

    }
    @Override
    public boolean sendMessageWithAudit( MessageSendDtoReq req) throws MessagingSystemException{

        if( (Constants.MESSAGE_CATEGORY_EMAIL.equals(req.getMessageCategoryId()) && Constants.CONTACT_TYPE_EMAIL.equals(req.getContactMethodTypeId()) )
                || SEND_MODE_TYPE_CUSTOM_MESSAGE_TYPE_TO_EMAIL.equalsIgnoreCase(req.getSendModeTypeId())) {
            emailService.sendEmail(req.getToEmailAddress(), req.getMessageTemplate(), req.getMessageSubject());
        }
        saveAuditMessage(req);
        return true;
    }
    @Override
    public void saveAuditMessage(MessageSendDtoReq req) throws MessagingSystemException{
        AuditMessageEntity auditMessageEntity = new AuditMessageEntity();
        auditMessageEntity.setEmployeeId(req.getEmployeeId());
        auditMessageEntity.setCreatedDatetime(new Date());
        auditMessageEntity.setMessageId(req.getId());
        auditMessageEntity.setMessageContent(req.getMessageTemplate());
        auditMessageEntity.setMessageSubject(req.getMessageSubject());
        auditMessageTypeRepository.save(auditMessageEntity);
    }
}
