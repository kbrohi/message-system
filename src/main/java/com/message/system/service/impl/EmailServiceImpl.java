package com.message.system.service.impl;

import com.message.system.config.ApplicationProperty;
import com.message.system.exception.MessagingSystemException;
import com.message.system.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.mail.Session;
import java.util.Properties;

@Service
public class EmailServiceImpl implements EmailService{

    @Autowired
    private ApplicationProperty applicationProperty;

    @Autowired
    private JavaMailSender javaMailSender;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);
    @Override
    public Boolean sendEmail(String emailAddress, String emailText, String emailSubject) throws MessagingSystemException{
        LOGGER.info("Sending Email Message To User : " + emailAddress + " with body : " + emailText);
        Boolean flag = Boolean.FALSE;

        if ("true".equals(applicationProperty.getEmailFlag()) && !StringUtils.isEmpty(emailAddress)) {
            SimpleMailMessage message = new SimpleMailMessage();
            try {
                //emailText = prepareEmailBodyWithHtmlFormat(emailText);

                //MimeMessage message = javaMailSender.createMimeMessage();

                message.setTo(emailAddress);
                message.setSubject(emailSubject);
                message.setText(emailText);
                message.setFrom(applicationProperty.getEmailFrom());

                javaMailSender.send(message);

                /*Session session = getNewSession(applicationProperty.getEmailHost(), applicationProperty.getEmailUserName(), applicationProperty.getEmailPassword(), applicationProperty.getEmailPort());

                Transport transport = session.getTransport("smtp");

                transport.connect(applicationProperty.getEmailHost(), applicationProperty.getEmailUserName(), applicationProperty.getEmailPassword());

                MimeMessage message = new MimeMessage(session);
                message.setSubject(emailSubject);

                InternetAddress from = new InternetAddress(applicationProperty.getEmailFrom());
                message.setFrom(from);
                message.addRecipients(Message.RecipientType.TO, emailAddress);

                Multipart multipart = new MimeMultipart("alternative");

                // Create the HTML Part
                BodyPart htmlBodyPart = new MimeBodyPart();// 4
                htmlBodyPart.setContent(emailText, "text/html"); // 5
                multipart.addBodyPart(htmlBodyPart); // 6

                // Set the Multipart's to be the email's content
                message.setContent(multipart);

                // Send message
                transport.sendMessage(message, message.getAllRecipients());*/
                flag = Boolean.TRUE;
            } catch (Exception e) {
                flag = Boolean.FALSE;
           /*     ///LOGGER.e("AddressException occured at EmailServiceImpl.sendEmail()", e);
            } catch (NoSuchProviderException e) {
                flag = Boolean.FALSE;
               // LOGGER.error("NoSuchProviderException occured at EmailServiceImpl.sendEmail()", e);
            } catch (MessagingException e) {*/
                flag = Boolean.FALSE;
                LOGGER.error("MessagingException occured at EmailServiceImpl.sendEmail()", e);
            }
            LOGGER.info("Email Message Sent successfully From Email Address : " + message.getFrom()+ " , To Address: " + emailAddress);
        }
        return flag;
    }
    private String prepareEmailBodyWithHtmlFormat(String emailBody) {
        StringBuilder updatedEmailBody = new StringBuilder();
        updatedEmailBody.append("<html>\n");
        updatedEmailBody.append("<body>\n");
        updatedEmailBody.append(emailBody);
        updatedEmailBody.append("</body>\n");
        updatedEmailBody.append("</html>");

        return updatedEmailBody.toString();
    }

    private Session getNewSession(String smtpHost, String smtpUsername, String smtpPassword, int smtpPort) {
        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.user", smtpUsername);
        props.put("mail.smtp.password", smtpPassword);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.auth", true);
        props.put("mail.debug", "true");
        props.setProperty("mail.transport.protocol", "smtp");
//        props.setProperty("mail.smtp.auth", "true");
 //       props.setProperty("mail.smtp.starttls.enable", "true");
  //      props.setProperty("mail.debug", "true");
        props.setProperty("mail.smtp.ssl.enable","true");
        props.setProperty("mail.test-connection","true");


        return Session.getDefaultInstance(props);
    }

}
