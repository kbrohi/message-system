package com.message.system.service.impl;

import com.message.system.dtos.AuditMessageDto;
import com.message.system.dtos.ContactMethodTypeLookupResDTO;
import com.message.system.dtos.EmployeeContactMethodDto;
import com.message.system.dtos.EmployeeDto;
import com.message.system.dtos.mapper.EmployeeMapper;
import com.message.system.exception.MessagingSystemException;
import com.message.system.model.*;
import com.message.system.repository.*;
import com.message.system.service.EmployeeService;
import com.message.system.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ContactMethodTypeRepository contactMethodTypeRepository;
    @Autowired
    private EmployeeContactMethodRepository contactMethodRepository;

    @Autowired
    private AuditMessageTypeRepository auditMessageTypeRepository;

    @Autowired
    private EmployeeTypeRepository employeeTypeRepository;
    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public EmployeeDto findBy(String employeeCode) throws MessagingSystemException {
        EmployeeDto employeeDto = null;
        Optional<EmployeeEntity> employeeEntityOpl = employeeRepository.findByEmployeeCode(employeeCode);
        if (employeeEntityOpl.isPresent()) {
            employeeDto = employeeMapper.employeeEntityToDto(employeeEntityOpl.get());
            setEmployeeTypeAndCompanyName(employeeDto);
        } else {
            throw new MessagingSystemException("Record Not Found");
        }
        return employeeDto;
    }

    private void setEmployeeTypeAndCompanyName(EmployeeDto employeeDto) {
        Optional<EmployeeTypeEntity> employeeTypeEntityOp = employeeTypeRepository.findById(employeeDto.getEmployeeTypeId());
        employeeDto.setEmployeeTypeName(employeeTypeEntityOp.isPresent() ? employeeTypeEntityOp.get().getEmployeeTypeName() : null);
        Optional<CompanyEntity> companyEntityOp = companyRepository.findById(employeeDto.getCompanyId());
        employeeDto.setCompanyName(companyEntityOp.isPresent() ? companyEntityOp.get().getName() : null);
    }

    @Override
    public List<EmployeeDto> findAllEmployeesByCompanyId(Long companyId) throws MessagingSystemException {
        List<EmployeeDto> employeeDtos = null;
        List<EmployeeEntity> employeeEntitiesOpl = employeeRepository.findAllByCompanyId(companyId);
        if (employeeEntitiesOpl !=null && employeeEntitiesOpl.size()>0) {
            employeeDtos = employeeMapper.listEmployeeEntityToDto(employeeEntitiesOpl);
            employeeDtos.forEach(e->{
                setEmployeeTypeAndCompanyName(e);
            });

        }else{
            throw  new MessagingSystemException("Record Not Found");
        }
        return employeeDtos;
    }
    @Override
    public List<EmployeeDto> findAll() throws MessagingSystemException {
        List<EmployeeDto> employeeDtos = null;
        List<EmployeeEntity> employeeEntitiesOpl = employeeRepository.findAll();
        if (employeeEntitiesOpl !=null) {
            employeeDtos = employeeMapper.listEmployeeEntityToDto(employeeEntitiesOpl);
            employeeDtos.forEach(e->{
                setEmployeeTypeAndCompanyName(e);
            });
        }else{
            throw  new MessagingSystemException("Record Not Found");
        }
        return employeeDtos;
    }
    @Override
    public List<ContactMethodTypeLookupResDTO> lookupContactMethodType() throws MessagingSystemException{
        List<ContactMethodTypeLookupResDTO>  typeLookupResDTOS = null;
        Optional<List<ContactMethodTypeLookupResDTO>> contactMethodTypeLookupResDTOSOpl =  contactMethodTypeRepository.findAllLookupByActiveInd();
        if(contactMethodTypeLookupResDTOSOpl.isPresent()){
            typeLookupResDTOS = contactMethodTypeLookupResDTOSOpl.get();
        }else{
            throw  new MessagingSystemException("Record Not Found");
        }
        return typeLookupResDTOS;
    }
    @Override
    public List<EmployeeContactMethodDto> contactMethodsById(String employeeCode) throws MessagingSystemException {
        List<EmployeeContactMethodDto> contactMethodDtos = null;
        Optional<List<EmployeeContactMethodEntity>> contactMethodEntitiesOpl = contactMethodRepository.findAllByEmployeeCode(employeeCode);
        if (contactMethodEntitiesOpl.isPresent()) {
            contactMethodDtos = employeeMapper.listContactMethodToDto(contactMethodEntitiesOpl.get());
            contactMethodDtos.forEach(e -> {
                Optional<ContactMethodTypeEntity> methodTypeEntityOp = contactMethodTypeRepository.findById(e.getContactMethodTypeId());
                e.setContactMethodTypeName(methodTypeEntityOp.isPresent() ? methodTypeEntityOp.get().getName() : null);
            });
        } else {
            throw new MessagingSystemException("Record Not Found");
        }
        return contactMethodDtos;
    }
    @Override
    public EmployeeContactMethodDto choosePrimaryContactMethod(Long employeeContactMethodId) throws MessagingSystemException{
        EmployeeContactMethodDto employeeContactMethodDto = null;
        Optional<EmployeeContactMethodEntity> contactMethodEntityOpl =  contactMethodRepository.findById(employeeContactMethodId);
        if(!contactMethodEntityOpl.isPresent()){
            throw new MessagingSystemException("Employee Contact Method Not Found against ["+employeeContactMethodId+"] ");
        }
        contactMethodRepository.markAllContactNonPrimary(employeeContactMethodId);
        EmployeeContactMethodEntity contactMethodEntity = contactMethodEntityOpl.get();
        contactMethodEntity.setPrimaryInd(true);
        contactMethodRepository.save(contactMethodEntity);
        employeeContactMethodDto =  new EmployeeContactMethodDto();
        employeeContactMethodDto.setStatus(Constants.STATUS_SUCCESS);
        return employeeContactMethodDto;
    }

    public List<AuditMessageDto> findAllMessages(String employeeCode) throws MessagingSystemException {

        List<AuditMessageDto> auditMessageDtos = null;

        List<AuditMessageEntity> auditMessageTypeEntities = auditMessageTypeRepository.findByEmployeeCode(employeeCode);
        auditMessageDtos = employeeMapper.listAuditMessageTypeEntityToDto(auditMessageTypeEntities);
        if(auditMessageDtos ==null || auditMessageDtos.size()==0){
            throw new MessagingSystemException("Record Not Found");
        }
        return auditMessageDtos;
    }
}
