package com.message.system.service;

import com.message.system.dtos.AuditMessageDto;
import com.message.system.dtos.ContactMethodTypeLookupResDTO;
import com.message.system.dtos.EmployeeContactMethodDto;
import com.message.system.dtos.EmployeeDto;
import com.message.system.exception.MessagingSystemException;

import java.util.List;

public interface EmployeeService {

    EmployeeDto findBy(String employeeCode) throws MessagingSystemException;
    List<EmployeeDto> findAll() throws MessagingSystemException;
    List<EmployeeDto> findAllEmployeesByCompanyId(Long companyId) throws MessagingSystemException;
    List<ContactMethodTypeLookupResDTO> lookupContactMethodType() throws MessagingSystemException;
    List<EmployeeContactMethodDto> contactMethodsById(String employeeCode) throws MessagingSystemException;
    EmployeeContactMethodDto choosePrimaryContactMethod(Long employeeContactMethodId) throws MessagingSystemException;

    List<AuditMessageDto> findAllMessages(String employeeCode) throws MessagingSystemException;

}
