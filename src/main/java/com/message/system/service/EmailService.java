package com.message.system.service;


import com.message.system.exception.MessagingSystemException;

public interface EmailService {

    Boolean sendEmail(String emailAddress, String emailText, String emailSubject) throws MessagingSystemException;
}
