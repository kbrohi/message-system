package com.message.system.repository;

import com.message.system.dtos.MessageSendDtoReq;
import com.message.system.model.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity,Long> {


 Optional<EmployeeEntity> findByEmployeeCode(String employeeCode);

 List<EmployeeEntity>  findAllByCompanyId(Long companyId);

 @Query(value = " SELECT NEW com.message.system.dtos.MessageSendDtoReq(e.companyId,e.id,e.employeeTypeId,ec.contactMethodValue,ec.contactMethodTypeId)" +
         " from EmployeeEntity e" +
         " LEFT JOIN EmployeeContactMethodEntity ec on ec.employeeId=e.id and ec.primaryInd='1' " +
         " where (:id is null OR e.id=:id)" +
         " AND (:companyId is null OR e.companyId=:companyId)" +
         " AND (:employeeTypeId is null OR e.employeeTypeId=:employeeTypeId) ")
 List<MessageSendDtoReq>  findAllByCompanyIdOrIdOrEmployeeTypeId(@Param("companyId") Long companyId, @Param("id")  Long id, @Param("employeeTypeId")  Long employeeTypeId);

}
