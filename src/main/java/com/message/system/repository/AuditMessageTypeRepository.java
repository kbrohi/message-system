package com.message.system.repository;

import com.message.system.model.AuditMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuditMessageTypeRepository extends JpaRepository<AuditMessageEntity,Long> {


    List<AuditMessageEntity> findByEmployeeId(Long employeeId);

    @Query(value = " SELECT AM FROM AuditMessageEntity AM left join EmployeeEntity E on E.id=AM.employeeId where E.employeeCode=:employeeCode")
    List<AuditMessageEntity> findByEmployeeCode(@Param("employeeCode") String employeeCode);
}
