package com.message.system.repository;

import com.message.system.dtos.MessageCategoryLookupResDTO;
import com.message.system.model.MessageCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageCategoryRepository extends JpaRepository<MessageCategoryEntity,Long> {


    @Query(value = "select new com.message.system.dtos.MessageCategoryLookupResDTO(mc.id,mc.messageCategoryName) from MessageCategoryEntity mc where mc.activeInd='1'")
    List<MessageCategoryLookupResDTO> lookupActive();
}
