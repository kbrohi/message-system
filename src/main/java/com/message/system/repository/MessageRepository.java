package com.message.system.repository;

import com.message.system.model.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MessageRepository extends JpaRepository<MessageEntity,Long> {

    List<MessageEntity> findAllByCompanyId(Long companyId);
    Optional<MessageEntity> findByMessageNameAndMessageCategoryId(String messageTypeName , Long messageCategoryId);

}
