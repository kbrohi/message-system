package com.message.system.repository;

import com.message.system.model.EmployeeContactMethodEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeContactMethodRepository extends JpaRepository<EmployeeContactMethodEntity,Long> {

    Optional<List<EmployeeContactMethodEntity>> findAllByEmployeeId(Long employeeId);


    @Query(value = " SELECT CM FROM EmployeeContactMethodEntity CM left join EmployeeEntity E on E.id = CM.employeeId " +
            " where E.employeeCode=:employeeCode ")
    Optional<List<EmployeeContactMethodEntity>> findAllByEmployeeCode(@Param("employeeCode") String employeeCode);

    @Transactional
    @Modifying
    @Query(value = "update EmployeeContactMethodEntity ec set ec.primaryInd='0' where ec.id <> :id")
    void markAllContactNonPrimary(@Param("id") Long id);
}
