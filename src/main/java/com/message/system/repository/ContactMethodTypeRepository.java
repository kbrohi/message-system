package com.message.system.repository;

import com.message.system.dtos.ContactMethodTypeLookupResDTO;
import com.message.system.model.ContactMethodTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactMethodTypeRepository extends JpaRepository<ContactMethodTypeEntity,Long> {


   List<ContactMethodTypeEntity> findAllByActiveInd(Long activeInd);

   @Query(value = "SELECT NEW com.message.system.dtos.ContactMethodTypeLookupResDTO(cm.id,cm.name) from ContactMethodTypeEntity cm where cm.activeInd='1' order by cm.name asc ")
   Optional<List<ContactMethodTypeLookupResDTO>> findAllLookupByActiveInd();

}
