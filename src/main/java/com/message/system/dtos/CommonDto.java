package com.message.system.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonDto {
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String message;
    //Success/Fail
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String status;


}
