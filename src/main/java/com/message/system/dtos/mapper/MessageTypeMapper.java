package com.message.system.dtos.mapper;

import com.message.system.dtos.MessageCategoryLookupResDTO;
import com.message.system.dtos.MessageDto;
import com.message.system.dtos.MessageSendDtoReq;
import com.message.system.model.MessageCategoryEntity;
import com.message.system.model.MessageEntity;
import org.mapstruct.*;

import java.util.List;


@Mapper(componentModel = "spring")
public interface MessageTypeMapper {



    List<MessageCategoryLookupResDTO> listMessageCategoryEntityToLookupDto(List<MessageCategoryEntity> e);

    MessageEntity messageTypeDtoToEntity(MessageDto e);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    void updateMessageTypeDtoToEntity(MessageDto e, @MappingTarget MessageEntity t);
    MessageDto messageTypeEntityToDto(MessageEntity e);
    List<MessageDto> listMessageTypeEntityToDto(List<MessageEntity> e);

    MessageSendDtoReq messageTypeEntityToSendDto(MessageEntity e);
}
