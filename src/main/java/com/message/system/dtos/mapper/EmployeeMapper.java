package com.message.system.dtos.mapper;

import com.message.system.dtos.AuditMessageDto;
import com.message.system.dtos.ContactMethodTypeLookupResDTO;
import com.message.system.dtos.EmployeeContactMethodDto;
import com.message.system.dtos.EmployeeDto;
import com.message.system.model.AuditMessageEntity;
import com.message.system.model.ContactMethodTypeEntity;
import com.message.system.model.EmployeeContactMethodEntity;
import com.message.system.model.EmployeeEntity;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;


@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    @Named(value = "contactMethodEntityToLookupDto")
    ContactMethodTypeLookupResDTO contactMethodEntityToLookupDto(ContactMethodTypeEntity e);
    @IterableMapping(qualifiedByName = "contactMethodEntityToLookupDto")
    List<ContactMethodTypeLookupResDTO> listContactMethodEntityToLookupDto(List<ContactMethodTypeEntity> e);

    EmployeeContactMethodDto contactMethodToDto(EmployeeContactMethodEntity e);

    List<EmployeeContactMethodDto> listContactMethodToDto(List<EmployeeContactMethodEntity> e);

    EmployeeDto employeeEntityToDto(EmployeeEntity e);
    List<EmployeeDto> listEmployeeEntityToDto(List<EmployeeEntity> es);

    AuditMessageDto auditMessageTypeEntityToDto(AuditMessageEntity e);
    List<AuditMessageDto> listAuditMessageTypeEntityToDto(List<AuditMessageEntity> es);
}
