package com.message.system.dtos.mapper;

import com.message.system.dtos.AuditMessageDto;
import com.message.system.dtos.CompanyDto;
import com.message.system.dtos.EmployeeDto;
import com.message.system.model.AuditMessageEntity;
import com.message.system.model.CompanyEntity;
import com.message.system.model.EmployeeEntity;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface CompanyMapper {

    CompanyDto companyEntityToDto(CompanyEntity e);
    List<CompanyDto> listCompanyEntityToDto(List<CompanyEntity> es);

    EmployeeEntity employeeDtoToEntity(EmployeeDto e);

    EmployeeDto employeeEntityToDto(EmployeeEntity e);
    List<EmployeeDto> listEmployeeEntityToDto(List<EmployeeEntity> e);

    AuditMessageDto auditMessageTypeEntityToDto(AuditMessageEntity e);
    List<AuditMessageDto> listAuditMessageTypeEntityToDto(List<AuditMessageEntity> e);
}
