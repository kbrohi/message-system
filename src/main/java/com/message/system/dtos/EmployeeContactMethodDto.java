package com.message.system.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeContactMethodDto  extends CommonDto{
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long id;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long employeeId;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long contactMethodTypeId;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String contactMethodTypeName;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String  contactMethodValue;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Boolean primaryInd;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Date createdDatetime;
}
