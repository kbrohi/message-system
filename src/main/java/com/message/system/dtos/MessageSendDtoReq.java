package com.message.system.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageSendDtoReq extends CommonDto{
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long id;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long companyId;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String employeeCode;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long employeeId;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long messageCategoryId;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long employeeTypeId;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String sendModeTypeId;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Long contactMethodTypeId;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String toEmailAddress;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Date messageSendTime;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String messageTemplate;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String messageSubject;

    public MessageSendDtoReq(Long companyId, Long employeeId, Long employeeTypeId, String toEmailAddress,Long contactMethodTypeId) {
        this.companyId = companyId;
        this.employeeId = employeeId;
        this.employeeTypeId = employeeTypeId;
        this.toEmailAddress = toEmailAddress;
        this.contactMethodTypeId = contactMethodTypeId;
    }
}
