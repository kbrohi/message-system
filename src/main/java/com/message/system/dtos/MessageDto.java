package com.message.system.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageDto extends CommonDto{
    private Long id;
    private Long companyId;
    private String companyName;
    private Long messageCategoryId;
    private String messageCategoryName;
    private String messageName;
    private String messageTemplate;
    private Boolean activeInd;
    private Date createdDatetime;
}
