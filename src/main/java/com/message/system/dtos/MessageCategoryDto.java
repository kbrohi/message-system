package com.message.system.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageCategoryDto extends CommonDto {
    private Long id;
    private Long msgCatId;
    private String messageCategoryName;
    private Boolean activeInd;
    private Date createdDatetime;

}
