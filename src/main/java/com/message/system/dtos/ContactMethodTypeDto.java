package com.message.system.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactMethodTypeDto extends CommonDto {
    private Long id;
    private Long cntMtdTypeId;
    private String name;
    private Boolean activeInd;
    private Date createdDatetime;
}
