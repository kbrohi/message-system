package com.message.system.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompanyDto extends CommonDto{
    private Long id;
    private Long cmpId;
    private String name;
    private String natureOfBusiness;
    private Date createdDatetime;
}
