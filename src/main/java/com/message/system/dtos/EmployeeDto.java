package com.message.system.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeDto  extends CommonDto{
    private Long id;
    private Long employeeTypeId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String employeeTypeName;
    private Long companyId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String companyName;
    private String fullName;
    private String employeeCode;
    private Date createdDatetime;
}
