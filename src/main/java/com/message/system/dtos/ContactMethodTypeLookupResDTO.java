package com.message.system.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactMethodTypeLookupResDTO {

    private Long id;
    private String name;

    public ContactMethodTypeLookupResDTO() {}



    public ContactMethodTypeLookupResDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
