package com.message.system.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeTypeDto  extends CommonDto{
    private Long id;
    private Long empTypeId;
    private String employeeTypeName;
    private Boolean activeInd;
    private Date createdDatetime;
}
