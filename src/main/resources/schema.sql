
DROP TABLE IF EXISTS Company;
Create table Company(
  id int AUTO_INCREMENT primary key,
  CMP_ID int,
  name varchar(100),
  nature_of_business varchar(500),
  ACTIVE_IND bit,
  created_datetime date
);

DROP TABLE IF EXISTS Employee_type;
Create table Employee_type(
  id int AUTO_INCREMENT primary key,
  EMP_TYP_ID int,
  employee_type_name varchar(100),
  active_ind bit,
  created_datetime date
);

DROP TABLE IF EXISTS Employee;
Create table Employee(
  id int AUTO_INCREMENT primary key,
  employee_type_id int,
  company_id int,
  full_name varchar(100),
  employee_code varchar(100),
  ACTIVE_IND bit,
  created_datetime date
);

DROP TABLE IF EXISTS contact_method_type;
Create table contact_method_type(
  id int AUTO_INCREMENT primary key,
  name varchar(100),
  CNT_MTD_TYP_ID int ,
  active_ind bit,
  created_datetime date
);
DROP TABLE IF EXISTS Employee_contact_method;
Create table Employee_contact_method(
  id int AUTO_INCREMENT primary key,
  employee_id int,
  contact_method_type_id int,
  CONTACT_METHOD_VALUE VARCHAR(100),
  primary_ind bit,
  ACTIVE_IND bit,
  created_datetime date
);

DROP TABLE IF EXISTS message_category;
Create table message_category(
  id int AUTO_INCREMENT primary key,
  MSG_CAT_ID int ,
  message_category_name varchar(100),
  active_ind bit,
  created_datetime date
);
DROP TABLE IF EXISTS message;

Create table message(
  id int AUTO_INCREMENT primary key,
  message_category_id int,
  COMPANY_ID int ,
  message_name varchar(100),
  message_subject varchar(5000),
  message_template varchar(5000),
  active_ind bit,
  created_datetime date
);

DROP TABLE IF EXISTS audit_message;
Create table audit_message(
  id int AUTO_INCREMENT primary key,
  message_id int,
  employee_id int,
  message_content varchar(5000),
  MESSAGE_SUBJECT varchar(5000),
  ACTIVE_IND bit,
  created_datetime date
);
