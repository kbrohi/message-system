package com.message.system;

import com.message.system.dtos.EmployeeDto;
import com.message.system.exception.MessagingSystemException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmployeeServiceImplTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void healthTest() throws Exception {
		Assert.hasText(this.restTemplate.getForObject("http://localhost:" + port + "/employee/health",
				String.class),"I'm Alive");
	}
	@Test
	public void findEmployeeCodeTest() throws MessagingSystemException{
		EmployeeDto emp = new EmployeeDto();
		ResponseEntity<EmployeeDto> responseEntity = restTemplate.getForEntity(
				"http://localhost:"+ port +"/employee/E001", EmployeeDto.class);
		emp = responseEntity.getBody();
		Assert.isTrue(emp.getEmployeeCode().equals("E001"),"Good to go Employee E001");
	}

}
