**Messaging System**
 
**To Run Application**
    prerequisite:
        ● Java 8 or above
        ● Maven 3+
     
   Database Scripts:  
    1. DLL scripts are available in schema.sql file on project root
    2. DML scripts are available in data.sql file on project root
   To Build: 
        execute common on project directory
            mvn clean install
     
    Once build succeed
     java -jar message-system-1.0-jar
     Or if you have open in eclipse/intellij idea/STS then just click on class MessageSystemApplication  run as java application
    
 
 
 **Entity Model**
     1. COMPANY     
     2. EMPLOYEE_TYPE
     3. EMPLOYEE
     4. CONTACT_METHOD_TYPE   -- for email/SMS for contact method type for employee
     5. EMPLOYEE_CONTACT_METHOD
     6. MESSAGE_CATEGORY -- Email/SMS for message Type -- note: do not confused with contact method types, they can be different
     7. MESSAGE_TYPE
     8. AUDIT_MESSAGE_TYPE   -- maintained  for logs 
     
The ERD diagram is on project root with file named MessageSystemERD.png, System_daigram.png, and Sequence_daigram.png ( message type sent )



 **API Details**
 
 APIs request/response is below
 
message category-lookup
http://localhost:8080/message-type/message-category-lookup
response
[
    {
        "id": 1,
        "name": "Email"
    },
    {
        "id": 2,
        "name": "SMS"
    }
]

create message type by company
URL POST http://localhost:8080/message-type/create-message-type

Request
{
    "id": null,
    "companyId": "1",
    "messageCategoryId": 1,
    "messageTypeName":"Company configured",
    "messageTemplate": "This sample email for adding as company",
    "messageSubject": "This Company Configured Email"
}

Response
{
    "id": 3,
    "messageCategoryId": 1,
    "messageTypeName": "Company configured",
    "messageTemplate": "This sample email for adding as company",
    "activeInd": null,
    "createdDatetime": null
 
}
update message type by company
URL PATCH http://localhost:8080/message-type/create-message-type
Request
{
    "id": 3,
    "messageCategoryId": 1,
    "messageTypeName": "Company configured",
    "messageTemplate": "This sample email for adding as company",
    "activeInd": false,
    "createdDatetime": "2021-01-30T22:20:44.530+00:00"
}
Response
{
    "id": 3,
    "messageCategoryId": 1,
    "messageTypeName": "Company configured",
    "messageTemplate": "This sample email for adding as company",
    "activeInd": false,
    "createdDatetime": "2021-01-30T22:20:44.530+00:00"
}


//////////////////////////////////////////////////////////
Employee 
////////////////////////////////
GET http://localhost:8085/employee/1
{
    "id": 1,
    "employeeTypeId": 1,
    "companyId": 1,
    "fullName": "John",
    "employeeCode": "A-02",
    "createdDatetime": "2021-01-30T19:00:00.000+00:00"
}

GET All contacts methods
URL http://localhost:8085/employee/1/contact-methods

[
    {
        "id": 1,
        "employeeId": 1,
        "contactMethodTypeId": 1,
        "primaryInd": true,
        "createdDatetime": "2021-01-30T19:00:00.000+00:00"
    },
    {
        "id": 2,
        "employeeId": 1,
        "contactMethodTypeId": 2,
        "primaryInd": false,
        "createdDatetime": "2021-01-30T19:00:00.000+00:00"
    }
]

URL PATCH http://localhost:8085/employee/choose-primary-contact-method/2

    Now get again all contact-methods to verify that systems has updated the contact-method to 2

//////////////////////////////////////////////////////////
Message Type 
////////////////////////////////
custom to address and custom email body
Request
{
    "id": null,
    "companyId": null,
    "employeeId": null,
    "employeeTypeId": null,
    "sendModeTypeId": "custom_message_type_to_email",
    "toEmailAddress": "kaleem.brohi27@gmail.com",
    "messageSendTime": null,
    "messageTemplate": "Hi this is testing email",
    "messageSubject": "Messaging Systems Alert"
}
Response
{
    "status": "Success",
    
}
send immediate
{
    "id": "1",
    "companyId": "1",
    "employeeId": null,
    "employeeTypeId": null,
    "sendModeTypeId": "immediate",
    "toEmailAddress": "kaleem.brohi27@gmail.com",
    "messageSendTime": null,
    "messageTemplate": null,
    "messageSubject": null
}
Response
{
    "message": "Send Message Types Request has been successfully sent",
    "status": "Success"
}

Scheduled
messageSendTime --- here goes scheduled time in ISO format


{
    "id": "1",
    "companyId": "1",
    "employeeId":null,
    "employeeTypeId": null,
    "sendModeTypeId": "nightly",
    "toEmailAddress": "kaleem.brohi27@gmail.com",
    "messageSendTime": "2021-01-30T23:10:00.000+00:00",
    "messageTemplate": null,
    "messageSubject": null
}
Response
{  
    "message": "Send Message Types Request has been successfully Scheduled",
    "status": "Success",
   
}